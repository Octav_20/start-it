package ro.orangetraining;

public abstract class Hotel_bedrooms implements MyInterface {
    private int d,h,w;

    public Hotel_bedrooms(int d, int h, int w) {
        this.d = d;
        this.h = h;
        this.w = w;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    @Override
    public int CalculateSIze() {
        int a=h*d*w;
        return a;
    }
    //public abstract void getStyle ();
    public void getStyle(String t1) {
        System.out.println(t1);
    }
}
