package ro.orangetraining;

public class GuessGame {
    //Player p1,p2,p3;

    public void startGame(){
        // procedura pricipala joc
        // instantiem jucatorii
        Player p1= new Player();
        Player p2= new Player();
        Player p3= new Player();
        //valorile ghicite
        int g_p1,g_p2,g_p3;
        //valori pentru status ghicit
        boolean corect_p1=false;
        boolean corect_p2=false;
        boolean corect_p3=false;
        // generez numarul target
        System.out.println("I'm thinking of a number between 0 and 9 ...");
        int target=(int) (Math.random()*10);
        while (true) {
            p1.guess();
            p2.guess();
            p3.guess();
            System.out.println("Number to guess was " + target);
            g_p1 = p1.numar;
            System.out.println("Player one guessed " + g_p1);
            g_p2 = p2.numar;
            System.out.println("Player one guessed " + g_p2);
            g_p3 = p3.numar;
            System.out.println("Player one guessed " + g_p3);
            if (g_p1==target){
                corect_p1=true;
            }
            if (g_p2==target){
                corect_p2=true;
            }
            if (g_p3==target){
                corect_p3=true;
            }
            if (corect_p1||corect_p2||corect_p3) {
                System.out.println("We have a winner! ");
                System.out.println("Player one got it right?" + corect_p1);
                System.out.println("Player two got it right?" + corect_p2);
                System.out.println("Player three got it right?" + corect_p3);
                //System.out.println("Game over");
                break;
            }
            else {
                //Nu a ghicit nimeni
                System.out.println("Players will have to try again.");
                target=(int) (Math.random()*10);

            }

        }




    }
}
